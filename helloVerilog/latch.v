module latch(q_out, data_in, enable) ;

    output q_out;
    input data_in, enable;

    assign q_out = enable ? data_in : q_out;

endmodule


module latch_reset(q_out, data_in, enable, reset) ;

    output q_out;
    input data_in, enable, reset;

    assign q_out = reset ? 0 : (enable ? data_in : q_out);

endmodule
