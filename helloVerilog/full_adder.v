module half_add (X, Y, S, C);
    input X, Y ;
    output S, C ;
    xor
        SUM (S, X, Y);
    and
        CARRY (C, X, Y);
endmodule

module full_add_struct (A, B, CI, S, CO) ;
    input A, B, CI ;
    output S, CO ;
    wire S1, C1, C2;
    // build full adder from 2 half-adders
    half_add
        PARTSUM(A, B, S1, C1),
        SUM(S1, CI, S, C2);
    // ... and an OR gate for the carry
    or
        CARRY (CO, C2, C1);
endmodule


module fa_rtl (A, B, CI, S, CO) ;
    input A, B, CI ;
    output S, CO ;
    // use continuous assignments
    assign S = A ^ B ^ CI;
    assign C0 = (A & B) | (A & CI) | (B & CI);
endmodule


module fa_bhv (A, B, CI, S, CO) ;
    input A, B, CI;
    output S, CO;
    reg S, CO;
    // use procedural assignments
    always@(A or B or CI)
        begin
            S = A ^ B ^ CI;
            CO = (A & B) | (A & CI) | (B & CI);
        end
endmodule
