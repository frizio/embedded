// Testbench for pacemaker timer module

`timescale 1ms/1ms //time unit and time precision 
module tb_pacemaker_timer;

// Inputs signal to the module under test
reg tb_clk;
reg tb_clr;

reg tb_t; // signal received from controller
// = 1 the controller has received the heartbeat from the ventriculum,
//     then reset the counter and z = 0;
// = 0 controller not receive the heartbeat.
//     Then increment the counter or if counter reach the limit, z = 1.

//Outputs
wire tb_z;

initial
begin
tb_t = 0;
tb_clk = 1'b1;
tb_clr = 1'b1;
#20 
tb_clr = 1'b0;
// Simulate sequence of signal receive from the controller
#80
tb_t = 1;
#100
tb_t = 1;
#100
tb_t = 1;
#100
tb_t = 0;
#100
tb_t = 1;
#100
tb_t = 1;
#100
tb_t = 0;
#1000
tb_t = 1;
#600000
$stop;
end

// Clock generation
always #50
tb_clk = ~tb_clk;

// The Module under test
//pacemaker_timer MUT(tb_clk, tb_clr, tb_t, tb_z); 
pacemaker_timer_two MUT(tb_clk, tb_clr, tb_t, tb_z);

endmodule
