// Pacemaker controller

`timescale 1ms/1ms    // directive necessary to suppress a warning in modelsim
module pacemaker_controller(clk ,clr, s, z, p, t);
    
    input clk,      // clock: to syncronize the fsm
          clr,      // clear: to reset the fsm
          s,        // heartbeat from the ventriculum
          z;        // counter limit reached (signal from timer)
    
    output reg p,   // pace: stimulus to the ventriculum
               t;   // signal to the timer (to reset the counter)

    reg[1:0] currentState, nextState;
        
    // States: 
    // S0=ResetTimer; S1=Wait; S2=Pace;
    parameter ResetTimer=2'b00, Wait=2'b01, Pace=2'b10;

    //reg[3:0] counter;

    // 0.8 seconds (75 bpm)
    //parameter SEC08=3'b111;
    
    
    // Define "next state" combinational logic
    always @(s, z, currentState)
           case (currentState)
               ResetTimer: 
                  begin
                     nextState = Wait; 
                  end
               Wait:
                  if (!s & !z) begin
                     nextState = Wait; end
                  else 
                     if (!s & z) begin
                        nextState = Pace; end
                     else 
                        if (s) begin
                           nextState = ResetTimer; end
               Pace: 
                  begin
                     nextState = ResetTimer; 
                  end
                  
             endcase
    
    // Define "state update" sequential logic
    always @(negedge clr, posedge clk)
      if (clr) 
         currentState <= ResetTimer;
      else
         currentState <= nextState;

   // Define "output" combinational logic
   always @(currentState)
      case (currentState)
          ResetTimer: 
             begin
                t = 1;
                p = 0;
             end
          Wait:
             begin
                t = 0;
                p = 0;
             end
          Pace: 
             begin
                t = 0;
                p = 1;  
             end
      endcase
      
endmodule