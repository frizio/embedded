// Pacemaker - Top module

`timescale 1ms/1ms    // directive necessary to suppress a warning in modelsim
module pacemaker_top(clk, clr, s, p);

    input clk, 
          clr, 
          s;    // Sensor to detect a natural contraction in heart's right ventricle
    output p;   // Stimulation sended to the heart's right ventricle

    wire t,      
         z;
    
    // Instantiation timer module
    //pacemaker_timer TIMER(clk, clr, t, z); 
    pacemaker_timer_two TIMER(clk, clr, t, z);

    // Instatiation controller module
    pacemaker_controller CONTROLLER(clk, clr, s, z, t, p);

endmodule
