// Testbench for pacemaker controller module

`timescale 1ms/1ms //time unit and time precision 
module tb_pacemaker_controller;

   // Inputs signal to the module under test
   reg tb_clk;
   reg tb_clr;
   
   reg tb_s; // Signal from heart's right ventricle
   reg tb_z; // Signal from the timer module (generate from here)

   // Outputs
   wire tb_p;   // Signal to the right ventricle
   wire tb_t;   // Signal to the timer module

   initial
   begin
      tb_clk = 1'b1;
      tb_clr = 1'b1;
      tb_s = 1'b0;
      tb_z = 1'b0;
      #20 
      tb_clr = 1'b0;
      #280
      tb_z = 1'b1;
      #100
      tb_z = 1'b0;
      #600000
      $stop;
   end

   // Clock generation
   always #50
      tb_clk = ~tb_clk;

   // The Module under test
   pacemaker_controller MUT(tb_clk, tb_clr, tb_s, tb_z, tb_p, tb_t); 
   
endmodule
