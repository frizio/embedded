// Pacemaker timer 2 version

`timescale 1ms/1ms    // directive necessary to suppress a warning in modelsim
module pacemaker_timer_two(clk ,clr, t, z);
 
 input clk,
       clr,
       t;
 
 output reg z;

 reg[1:0] state;
 
 reg[3:0] counter;
 
 parameter ResetTimer=2'b00, Wait=2'b01, Pace=2'b10;
 
 // 0.8 seconds (75 bpm)
 parameter SEC08=3'b111;
 
 always @(posedge clk or posedge clr)
   if (clr) 
      begin
         state <= ResetTimer;
         counter <= 0;
         z <= 0;
      end
   else
     case (state)
         ResetTimer:
            if (t) 
               begin
                 counter <= 0;
                 state <= Wait;
                 z <= 0;
               end
             else
                 z <= 0;
         Wait:
            if (t) 
               begin
                  state <= ResetTimer;
                  z <= 0;
               end
             else
               if (counter==SEC08) 
                  begin
                      state <= ResetTimer;
                      z <= 1;
                  end
               else
                  begin
                     counter <= counter + 1;
                     state <= Wait;
                     z <= 0;
                  end
    endcase

endmodule

