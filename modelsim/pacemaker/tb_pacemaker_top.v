// Testbench for pacemaker top module

`timescale 1ms/1ms //time unit and time precision 
module tb_pacemaker_top;

   // Inputs signal to the module under test
   reg tb_clk;
   reg tb_clr;
   reg tb_s;

   // Outputs
   wire tb_p;

   initial
      begin
         tb_clk = 1'b1;
         tb_clr = 1'b1;

         tb_s = 1'b1;

         #20 
         tb_clr = 1'b0;

         #80
         tb_s = 1'b1;

         #200
         tb_s = 1'b0;

         #100
         tb_s = 1'b1;

         #100
         tb_s = 1'b0;
         
         #1000
         tb_s = 1'b0;

#600000
$stop;
end

// Clock generation
always #50
tb_clk = ~tb_clk;

// The Module under test
pacemaker_top MUT(tb_clk, tb_clr, tb_s, tb_p); 

endmodule
