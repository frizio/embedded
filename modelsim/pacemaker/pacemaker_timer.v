// Pacemaker timer
// Note: this version seem don't increment the counter

`timescale 1ms/1ms    // directive necessary to suppress a warning in modelsim
module pacemaker_timer(clk ,clr, t, z);
    
    input clk,
          clr,
          t;
    
    output reg z;
   
    //reg[1:0] state;
    reg[1:0] currentState, nextState;
    
    reg[3:0] counter;
    //reg[3:0] currentCounter, nextCounter;
    
    parameter ResetTimer=2'b00, Wait=2'b01, Pace=2'b10;
    
    // 0.8 seconds (75 bpm)
    parameter SEC08=3'b111;
    
    
    // Define "next state" combinational logic
    always @(t, currentState)
           case (currentState)
               ResetTimer: 
                  begin
                     nextState = Wait; 
                  end
               Wait:
                  if (t) begin
                     nextState = ResetTimer; end
                  else begin
                     nextState = Wait; end       
           endcase
    
    // Define "state update" sequential logic
    always @(negedge clr, posedge clk)
      if (clr) 
         currentState <= ResetTimer;
      else
         currentState <= nextState;
    
    // Define "output" combinational logic
    always @(currentState)
       case (currentState)
           ResetTimer: 
              begin
                 z = 0;
                 counter = 0;
              end
           Wait:
              begin
                 if (counter==SEC08)
                    z = 1;
                  else 
                     begin
                         counter <= counter + 1'b1;
                         z = 0;
                     end
              end
       endcase

endmodule
