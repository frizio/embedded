/******************************************
** Moore type FSM, version 3             **
** Detect patterns 110, 101 with overlap **
******************************************/
// "Next State" specification and "state update" in the same always block

module Moore3(clk, Clr_, w, z);
    
    input clk, Clr_, w;
    output z;      // In this case output is a net 
    
    // State variable
    // Directly update the current state with the next state.
    reg [2:0] y;
    
    // State assignments
    localparam A=3'b000, B=3'b001, C=3'b010, 
               D=3'b011, E=3'b100, F=3'b101;
               
    // Define "next state" and "output" combinational logic
    always @(negedge Clr_, posedge clk)
       if (!Clr_) 
          y <= A;   // Reset
       else
          //y <= Y;   // Flip Flop Update
          case (y)
              A: if (w) y <= B;
                 else   y <= A;
              B: if (w) y <= C;
                 else   y <= E;
              C: if (w) y <= C;
                 else   y <= D;
              D: if (w) y <= F;
                 else   y <= A;
              E: if (w) y <= F;
                 else   y <= A;
              F: if (w) y <= C;
                 else   y <= E;
              default:  y <= 3'bxxx;
          endcase
       
       // Define output combinational logic;
       assign z = (y==D & y==F) ? 1 : 0;
    
endmodule // Moore3
