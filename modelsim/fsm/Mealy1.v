/******************************************
** Mealy type FSM, version 1             **
** Detect patterns 110, 101 with overlap **
******************************************/

module Mealy1(clk, Clr_, w, z);
    
    input clk, Clr_, w;
    output reg z;
    
    reg [1:0] y, Y;
    
    // State assignments
    localparam A=3'b00, B=3'b01, 
               C=3'b10, D=3'b11;
    
    // Define "next state" and "output" combinational logic.
    always @(w, y)
       case (y)
           A: if (w) begin
                 Y = B; z = 0; end
              else begin
                 Y = A; z = 0; end
           B: if (w) begin
                 Y = C; z = 0; end
              else begin
                 Y = D; z = 0; end
           C: if (w) begin
                 Y = C; z = 0; end
              else begin
                 Y = D; z = 1; end
           D: if (w) begin
                 Y = B; z = 1; end
              else begin
                 Y = A; z = 0; end
         endcase
         
    // Define "state update" sequential logic
    always @(negedge Clr_, posedge clk)
       if (!Clr_) 
          y <= A;
       else
          y <= Y;
    
endmodule // Mealy1

