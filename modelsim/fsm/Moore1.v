/******************************************
** Moore type FSM, version 1             **
** Detect patterns 110, 101 with overlap **
******************************************/

`timescale 1ns/1ns
module Moore1(clk, Clr_, w, z);
    
    input clk,     // clock: to syncronize the fsm
          Clr_,    // Clear: (neg) to reset the fsm
          w;       // Input binary sequence
          
    output reg z;  //  
    
    // State variables
    reg [2:0] y,    // Current state 
              Y;    // Next state
    
    // State assignments
    localparam A=3'b000, B=3'b001, C=3'b010, 
               D=3'b011, E=3'b100, F=3'b101;
               
    // Define "next state" combinational logic
    // This isn't yet what in the next state happens, 
    // but only the specification of the next state.
    // This depending on the current state and the input.
    always @(y, w)
       case (y)
           A: if (w) Y = B;
              else   Y = A;
           B: if (w) Y = C;
              else   Y = E;
           C: if (w) Y = C;
              else   Y = D;
           D: if (w) Y = F;
              else   Y = A;
           E: if (w) Y = F;
              else   Y = A;
           F: if (w) Y = C;
              else   Y = E;
           default:  Y = 3'bxxx;
       endcase
    
    // Define "state update" sequential logic
    always @(negedge Clr_, posedge clk)
       if (!Clr_) 
          y <= A;
       else
          y <= Y;
    
    // Define "output" combinational logic
    always @(y)
       if (y==D | y==F) 
          z = 1;
       else
          z = 0;
    
endmodule // Moore1
