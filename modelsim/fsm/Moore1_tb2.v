/************************************
** Testbench 2v for Moore1 module **
************************************/
`timescale 1ns/1ns
module Moore1_tb2();

   reg clk;
   reg Clr_;
   reg s;
   reg[15:0] w;
   wire z;
   integer i;

   initial begin
      clk = 1'b0;
      Clr_ = 1'b0;
      w = 16'b00110101001011100;
      s = 0;
      i = 0;
   
      #7 Clr_ = 1'b1;   
   end 

   always #5 
      clk = ~clk;
      
   always @(posedge clk) begin
       s = w >> i;
       i = i+1;
   end
       
   

Moore1 MUT(clk, Clr_, s, z);

endmodule // Moore1_tb()
