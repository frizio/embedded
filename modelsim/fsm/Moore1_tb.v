/************************************
** Testbench for Moore1 module **
************************************/
`timescale 1ns/1ns
module Moore1_tb();

reg clk;
reg Clr_;

reg w;
wire z;

initial begin
   clk = 1'b0;
   Clr_ = 1'b0;
   
   //w = 17'b00110101001011100;
   w = 1'b0;
   
   #7 Clr_ = 1'b1;
   
   #10 w = 1'b0;
   #10 w = 1'b1;
   #10 w = 1'b1;
   #10 w = 1'b0;
   #10 w = 1'b1;
   #10 w = 1'b0;
   #10 w = 1'b1;
   #10 w = 1'b0;
   #10 w = 1'b0;
   #10 w = 1'b1;
   #10 w = 1'b0;
   #10 w = 1'b1;
   #10 w = 1'b1;
   #10 w = 1'b1;
   #10 w = 1'b0;
   #10 w = 1'b0;
end 

always #5 
   clk = ~clk;

//
//always @(posedge clk) 
   

Moore1 MUT(clk, Clr_, w, z);

endmodule // Moore1_tb()
