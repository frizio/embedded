/******************************************
** Moore type FSM, version 2             **
** Detect patterns 110, 101 with overlap **
******************************************/
// "Next State" and "Output" (computational logic) in the same procedural block

module Moore2(clk, Clr_, w, z);
    
    input clk, Clr_, w;
    output reg z;  //  
    
    // State variables
    reg [2:0] y,    // Current state 
              Y;    // Next state
    
    // State assignments
    localparam A=3'b000, B=3'b001, C=3'b010, 
               D=3'b011, E=3'b100, F=3'b101;
               
    // Define "next state" and "output" combinational logic
    always @(y, w) begin
       // Next state
       case (y)
           A: if (w) Y = B;
              else   Y = A;
           B: if (w) Y = C;
              else   Y = E;
           C: if (w) Y = C;
              else   Y = D;
           D: if (w) Y = F;
              else   Y = A;
           E: if (w) Y = F;
              else   Y = A;
           F: if (w) Y = C;
              else   Y = E;
           default:  Y = 3'bxxx;
       endcase
       // Output
       if (y==D | y==F) 
          z = 1;
       else
          z = 0;
   end
       
    
    // Define "state update" sequential logic
    always @(negedge Clr_, posedge clk)
       if (!Clr_) 
          y <= A;   // Reset
       else
          y <= Y;   // Flip Flop Update
    
endmodule // Moore2
