// Traffic lights controller (blocking version)

`timescale 10ms/10ms    // directive necessary to suppress a warning in modelsim
module traffic_lights_ct_blocking(clk ,clr, lightRNS, lightYNS, lightGNS, lightREW, lightYEW, lightGEW);
    
    input clk;      // clock: to syncronize the fsm
    input clr;      // Clear: to reset the fsm

    output reg lightRNS, lightYNS, lightGNS, 
               lightREW, lightYEW, lightGEW;

    reg[2:0] state, 
             next_state; // added next_state
    
    reg[3:0] count, next_count;

    /*
     States: 
    S0= Green_NS and Red_EW, S1=Yellow_NS and Red_EW,  S2=Red_NS and Red_EW
    S3= Red_NS and Green_EW, S4= Red_NS and Yellow_EW, S5= Red_NS and Red_EW 
    */
    parameter S0=3'b000 , S1=3'b001, S2=3'b010, 
              S3=3'b011 , S4=3'b100 , S5=3'b101;

    // Delays 5sec and 1 sec
    parameter SEC5=4'b1010, 
              SEC1=4'b0010 ; 

    // "State Update" sequential logic
    always @(posedge clk or posedge clr)
    begin
        if (clr == 1)
            begin
                state <= S0;
                count <= 0;
            end   
        else 
            begin
                state <= next_state;
                count <= next_count;
            end
    end
      
    
    // "Next state" combinational 
    // next state and next count logic            
    always@(state, count)
    begin
        case (state)
            S0:
                if (count==SEC5)
                    begin
                        count = 0;
                        next_count = count;
                        next_state = S1;
                    end
                else
                    begin
                        count = count + 1;
                        next_count = count;
                        next_state = S0;
                    end
            S1: 
                if (count==SEC1)
                    begin
                        count = 0;
                        next_count = count;
                        next_state = S2;
                    end
                else
                    begin
                        count = count + 1;
                        next_count = count;
                        next_state = S1;
                    end
            S2: 
                if (count==SEC1)
                    begin
                        count = 0;
                        next_count = count;
                        next_state = S3;
                    end
                else
                    begin
                        count = count + 1;
                        next_count = count;
                        next_state = S2;
                    end
            S3: 
                if (count==SEC5)
                    begin
                        count = 0;
                        next_count = count;
                        next_state = S4;
                    end
                else
                    begin
                        count = count + 1;
                        next_count = count;
                        next_state = S3;
                    end
            S4: 
                if (count==SEC1)
                    begin
                        count = 0;
                        next_count = count;
                        next_state = S5;
                    end
                else
                    begin
                        count = count + 1;
                        next_count = count;
                        next_state = S4;
                    end
            S5: 
                if (count==SEC1)
                    begin
                        count = 0;
                        next_count = count;
                        next_state = S0;
                    end
                else
                    begin
                        count = count + 1;
                        next_count = count;
                        next_state = S5;
                    end
            default: 
                begin
                    next_state = S0; // 4'b0xxx;
                    next_count = 0;
                end
        endcase
    end
     
    // Output combinational logic
    always @(*)
        begin
            //lightGNS=1'b0; lightYNS=1'b0; lightRNS=1'b1; //Initialization
            //lightGEW=1'b0; lightYEW=1'b0; lightREW=1'b1; 
            lightGNS=0; lightYNS=0; lightRNS=0;
            lightGEW=0; lightYEW=0; lightREW=0;
            case(state)
                S0: 
                    begin
                        lightGNS=1; lightYNS=0; lightRNS=0;
                        lightGEW=0; lightYEW=0; lightREW=1; 
                    end
                S1: 
                    begin
                        lightGNS=0; lightYNS=1; lightRNS=0;
                        lightGEW=0; lightYEW=0; lightREW=1;
                    end
                S2: 
                    begin
                        lightGNS=0; lightYNS=0; lightRNS=1;
                        lightGEW=0; lightYEW=0; lightREW=1;
                    end
                S3: 
                    begin
                        lightGNS=0; lightYNS=0; lightRNS=1;
                        lightGEW=1; lightYEW=0; lightREW=0;
                    end
                S4: 
                    begin
                        lightGNS=0; lightYNS=0; lightRNS=1;
                        lightGEW=0; lightYEW=1; lightREW=0;
                    end
                S5: 
                    begin
                        lightGNS=0; lightYNS=0; lightRNS=1;
                        lightGEW=0; lightYEW=0; lightREW=1;
                    end
                default: 
                    begin
                    lightGNS=0; lightYNS=0; lightRNS=1;
                    lightGEW=0; lightYEW=0; lightREW=1;
                    end
            endcase
        end

endmodule

