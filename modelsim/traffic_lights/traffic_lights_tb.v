// Testbench for traffic_lights

`timescale 10ms/10ms //time unit and time precision 

module traffic_lights_tb;

    // Inputs signal to the module under test
    reg t_clk;
    reg t_clr;
 
    //Outputs
    wire R_RNS, R_YNS, R_GNS, R_REW, R_YEW, R_GEW;
    
 
    initial
        begin
            t_clk = 1'b1;
            t_clr = 1'b1;
            #2 
            t_clr = 1'b0;
            #60000 
            $stop;      // Stop simulation after 60002ms=60 seconds
        end

    // Clock generation
    always #25
        t_clk = ~t_clk;

    // The Module under test
    traffic_lights_top MUT(t_clk, t_clr, R_RNS, R_YNS, R_GNS, R_REW, R_YEW, R_GEW); 

endmodule

