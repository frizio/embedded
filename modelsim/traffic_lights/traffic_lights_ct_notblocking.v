// Traffic lights controller (NOT blocking version)

`timescale 10ms/10ms    // directive necessary to suppress a warning in modelsim
module traffic_lights_ct_notblocking(clk ,clr, lightRNS, lightYNS, lightGNS, lightREW, lightYEW, lightGEW);
    
    input clk;      // clock: to syncronize the fsm
    input clr;      // Clear: to reset the fsm

    output reg lightRNS, lightYNS, lightGNS, 
               lightREW, lightYEW, lightGEW;

    reg[2:0] state;
    
    reg[3:0] counter;

    
    // States: 
    // S0=Green_NS and Red_EW, S1=Yellow_NS and Red_EW,  S2=Red_NS and Red_EW
    // S3=Red_NS and Green_EW, S4= Red_NS and Yellow_EW, S5= Red_NS and Red_EW 
    parameter S0=3'b000, S1=3'b001, S2=3'b010, 
              S3=3'b011, S4=3'b100, S5=3'b101;

    // Delays:
    // 5sec and 1 sec
    parameter SEC5=4'b1001, 
              SEC1=4'b0001; 


    // "State Update" sequential logic and
    // "Next state" combinational logic
    always @(posedge clk or posedge clr)
    begin
        if (clr == 1)
            begin
                state <= S0;
                counter <= 0;
            end   
        else 
            case (state)
                S0:
                    if (counter==SEC5)
                        begin
                            counter <= 0;
                            state <= S1;
                        end
                    else
                        begin
                            counter <= counter + 1;
                            state <= S0;
                        end
                S1: 
                    if (counter==SEC1)
                        begin
                            counter <= 0;
                            state <= S2;
                        end
                    else
                        begin
                            counter <= counter + 1;
                            state <= S1;
                        end
                S2: 
                    if (counter==SEC1)
                        begin
                            counter <= 0;
                            state <= S3;
                        end
                    else
                        begin
                            counter <= counter + 1;
                            state <= S2;
                        end
                S3: 
                    if (counter==SEC5)
                        begin
                            counter <= 0;
                            state <= S4;
                        end
                    else
                        begin
                            counter <= counter + 1;
                            state <= S3;
                        end
                S4: 
                    if (counter==SEC1)
                        begin
                            counter <= 0;
                            state <= S5;
                        end
                    else
                        begin
                            counter <= counter + 1;
                            state <= S4;
                        end
                S5: 
                    if (counter==SEC1)
                        begin
                            counter <= 0;
                            state <= S0;
                        end
                    else
                        begin
                            counter <= counter + 1;
                            state <= S5;
                        end
                default: 
                    begin
                        state <= S0;
                    end
            endcase
    end


    // Output combinational logic
    always @(*)
        begin

            lightGNS=1; lightYNS=0; lightRNS=0; 
            lightGEW=0; lightYEW=0; lightREW=1;

            case(state)
                S0:                 // Green_NS and Red_EW
                    begin
                        lightGNS=1; lightYNS=0; lightRNS=0;
                        lightGEW=0; lightYEW=0; lightREW=1; 
                    end
                S1:                 // Yellow_NS and Red_EW
                    begin
                        lightGNS=0; lightYNS=1; lightRNS=0;
                        lightGEW=0; lightYEW=0; lightREW=1;
                    end
                S2:                 // Red_NS and Red_EW
                    begin
                        lightGNS=0; lightYNS=0; lightRNS=1;
                        lightGEW=0; lightYEW=0; lightREW=1;
                    end
                S3:                 // Red_NS and Green_EW
                    begin
                        lightGNS=0; lightYNS=0; lightRNS=1;
                        lightGEW=1; lightYEW=0; lightREW=0;
                    end
                S4:                 // Red_NS and Yellow_EW
                    begin
                        lightGNS=0; lightYNS=0; lightRNS=1;
                        lightGEW=0; lightYEW=1; lightREW=0;
                    end
                S5:                 // Red_NS and Red_EW
                    begin
                        lightGNS=0; lightYNS=0; lightRNS=1;
                        lightGEW=0; lightYEW=0; lightREW=1;
                    end
                default: 
                    begin
                        lightGNS=0; lightYNS=0; lightRNS=1;
                        lightGEW=0; lightYEW=0; lightREW=1;
                    end
            endcase
        end

endmodule

