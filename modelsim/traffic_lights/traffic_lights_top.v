// Traffic lights - Top module

`timescale 10ms/10ms    // directive necessary to suppress a warning in modelsim
module traffic_lights_top(clk, clr, RNS, YNS, GNS, REW, YEW, GEW);

    input clk, clr;
    output RNS, YNS, GNS, REW, YEW, GEW; // lights

    // Istatiation controller module
    //traffic_lights_ct_blocking U1(clk, clr, RNS, YNS, GNS, REW, YEW, GEW); // not work :(
    traffic_lights_ct_notblocking U1(clk, clr, RNS, YNS, GNS, REW, YEW, GEW);
    

endmodule

