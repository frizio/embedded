/************************************
** Testbench 2v for and_proc module *
************************************/
`timescale 1ns/1ns
module and_count_tb2();
    
    reg [3:0] a, b;
    wire [3:0] f;
    
    initial begin
        a = 4'b0000;
        b = 4'b0000;
        #20
        a = 4'b1111;
        b = 4'b0101;
        #20
        a = 4'b1100;
        b = 4'b1111;
        #20
        a = 4'b1100;
        b = 4'b0011;
        #20
        a = 4'b1100;
        b = 4'b1010;
        //#20
        //$finish;
    end
    
    and_cont MUT(f, a, b);
    
endmodule // and_cont_tb2
