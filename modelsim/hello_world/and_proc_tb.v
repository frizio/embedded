/************************************
** Testbench for and_proc module **
************************************/
`timescale 1ns/1ns
module and_proc_tb();
    
    reg clk;
    reg [1:0] counter;
    wire f;
    
    initial begin
       clk = 1;
       counter = -1;
    end 
    
    always #5 
       clk = ~clk;
    
    always @(posedge clk) 
       counter = counter + 1;

    and_proc MUT(f, counter[1], counter[0]);
    
endmodule // and_proc_tb
