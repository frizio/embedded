/****************************
** Behaviour Specification **
** Continuous Assignment   **
****************************/
module and_cont(f, x, y);
    
    input x, y;
    output f;
    
    // Just assign to the output 
    // the boolean function that we want to be computed.
    assign f = ~x & y;
    
endmodule // and_cont
