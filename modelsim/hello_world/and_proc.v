/****************************
** Behaviour Specification **
** Procedural Assignment   **
****************************/
module and_proc(f, x, y);

    input x, y;
    
    // Remember: in the continuous assignment we can assigned wires to the output.
    // But, here we don't have continuous output,
    // we have to actually store the output 
    // (that is computed inside the always block).
    // Then store the result until it gets computed for the next time.
    output reg f;
    
    //      this is the sensitivity list
    always @(x, y)
       if (x==0 & y==1)
          f = 1'b1;
       else
          f = 1'b0;
    
endmodule // and_proc
