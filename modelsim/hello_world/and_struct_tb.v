/************************************
** Testbench for and_struct module **
************************************/
/************************************************************************
** Create the signals that can be fed into what we want to test
** and that we have the capability of displaying what comes out of that
************************************************************************/

// Time is involved:
// First it's necessary specify a timescale (default is 1 picosecond).
// The timescale at which things are happening is 1ns, and the internal resolution is 1ns
// It's possible use smaller internal increment (0.1ns for example),
// if we need to find, for some reason, a resolution for some internal computation.
`timescale 1ns/1ns

// No need input: it's a generator module.
module and_struct_tb();
    
    // Generate the clock:
    // It's something that will have to have a memory, so need a register.
    reg clk;
    
    reg [1:0] counter;
    wire f;
    
    // Procedural (initial) block:
    // Run only at the beginning, when the simulation starts.
    initial begin
       clk = 1;
       counter = -1;    // preset to -1 before simulation start
    end 
    
    // Another procedural block
    // What do when something happened?  
    // Every 5 unit time invert the value of the clock
    always #5 
       clk = ~clk;
    
    // Block 2
    // This block is "sensitive" to the clock
    // posedge: the counter react at the level of the transiction of clock signal
    // negedge: the counter react at the level of the clock signal 
    // This block is sensitive to the positive edge of the clock
    always @(posedge clk) 
       counter = counter + 1;
       
    // Now comes to point where we actually looking at 
    // the module that we want to test.
    // Get a specific istantiation of the module:
    // MUT stands for "Module Under Test"
    and_struct MUT(f, counter[1], counter[0]);
    
endmodule // end and_struct_tb
