/***************************
** Structural description **
***************************/

//`timescale 1ns/1ns
module and_struct(f, x, y);
    
    input x, y;
    output f;
    wire a;
    
    // Use the GATE explicitly and WIRE goes uè
    not(a, x);
    and(f, a, y);    
    
endmodule // and_struct

/******************************************************************
** If compile success, need to test that this really working right.
** To do this, it's necessary write the testbench.
** This will be another module that gives the necessary input
** (or create the necessary input for x and y)
** and then displays what comes out in the form of f.
*******************************************************************/
