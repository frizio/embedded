
module and_struct ( f, x, y );
  input x, y;
  output f;
  wire   n2;

  NR2XD2 U3 ( .A1(n2), .A2(x), .ZN(f) );
  INVD12 U4 ( .I(y), .ZN(n2) );
endmodule

