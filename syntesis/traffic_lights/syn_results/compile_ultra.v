
module traffic_lights_ct_notblocking ( clk, clr, lightRNS, lightYNS, lightGNS, 
        lightREW, lightYEW, lightGEW );
  input clk, clr;
  output lightRNS, lightYNS, lightGNS, lightREW, lightYEW, lightGEW;
  wire   counter_1_, N100, n34, n35, n36, n37, n6, n10, n168, n83, n84, n85,
         n86, n87, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n123,
         n124, n125, n126, n127, n128, n129, n130, n131, n132, n133, n134,
         n135, n136, n137, n138, n139, n140, n141, n142, n143, n144, n145,
         n146, n147, n148, n149, n150, n151, n152, n153, n154, n155, n156,
         n157, n158, n159, n160, n161, n162, n163, n164, n165, n166, n167;

  DFCNQD4 state_reg_2_ ( .D(N100), .CP(clk), .CDN(n167), .Q(n10) );
  DFCNQD4 counter_reg_1_ ( .D(n35), .CP(clk), .CDN(n167), .Q(counter_1_) );
  DFCNQD4 state_reg_0_ ( .D(n166), .CP(clk), .CDN(n167), .Q(n6) );
  DFCND4 counter_reg_0_ ( .D(n36), .CP(clk), .CDN(n167), .Q(n100), .QN(n86) );
  DFCND4 counter_reg_2_ ( .D(n34), .CP(clk), .CDN(n167), .Q(n99) );
  DFCND4 state_reg_1_ ( .D(n165), .CP(clk), .CDN(n167), .Q(n98), .QN(n85) );
  DFCND2 counter_reg_3_ ( .D(n37), .CP(clk), .CDN(n167), .Q(n93), .QN(n94) );
  CKND4 U3 ( .I(n119), .ZN(n148) );
  CKND8 U4 ( .I(n151), .ZN(n111) );
  NR2D1 U5 ( .A1(n91), .A2(n93), .ZN(n127) );
  CKND2D1 U6 ( .A1(n98), .A2(n151), .ZN(n154) );
  INVD0 U7 ( .I(n100), .ZN(n143) );
  ND2D2 U8 ( .A1(n100), .A2(n131), .ZN(n156) );
  ND3D4 U9 ( .A1(n99), .A2(n100), .A3(counter_1_), .ZN(n117) );
  CKBD0 U10 ( .I(n113), .Z(n83) );
  INVD2 U11 ( .I(n87), .ZN(n84) );
  CKND4 U12 ( .I(n86), .ZN(n87) );
  CKND4 U13 ( .I(n143), .ZN(n114) );
  CKND2D2 U14 ( .A1(n114), .A2(n139), .ZN(n134) );
  ND2D2 U15 ( .A1(n95), .A2(n102), .ZN(n146) );
  CKND2D4 U16 ( .A1(n103), .A2(n95), .ZN(n161) );
  CKND4 U17 ( .I(n152), .ZN(n95) );
  ND3D2 U18 ( .A1(n113), .A2(n114), .A3(n139), .ZN(n140) );
  CKND2D2 U19 ( .A1(n111), .A2(n98), .ZN(n139) );
  INVD2 U20 ( .I(n147), .ZN(n144) );
  CKND3 U21 ( .I(n152), .ZN(n108) );
  CKND2D4 U22 ( .A1(n108), .A2(n120), .ZN(n107) );
  INR2D2 U23 ( .A1(n148), .B1(n159), .ZN(n150) );
  INVD3 U24 ( .I(lightGEW), .ZN(n159) );
  INVD2 U25 ( .I(n111), .ZN(n104) );
  OAI221D2 U26 ( .A1(n144), .A2(n112), .B1(n148), .B2(n159), .C(n160), .ZN(
        N100) );
  ND2D2 U27 ( .A1(n158), .A2(n157), .ZN(n166) );
  NR2XD2 U28 ( .A1(n107), .A2(n111), .ZN(lightGEW) );
  INVD1 U29 ( .I(n164), .ZN(lightYNS) );
  OA21D1 U30 ( .A1(n126), .A2(n84), .B(n92), .Z(n97) );
  CKND2 U31 ( .I(n160), .ZN(lightYEW) );
  ND2D2 U32 ( .A1(n120), .A2(n85), .ZN(n162) );
  CKND3 U33 ( .I(n151), .ZN(n103) );
  CKND0 U34 ( .I(n145), .ZN(lightRNS) );
  CKND2D0 U35 ( .A1(n152), .A2(n151), .ZN(n168) );
  CKND3 U36 ( .I(n142), .ZN(n89) );
  INVD4 U37 ( .I(n89), .ZN(n90) );
  CKND4 U38 ( .I(n131), .ZN(n113) );
  CKXOR2D1 U39 ( .A1(n140), .A2(n141), .Z(n34) );
  CKND1 U40 ( .I(n110), .ZN(n112) );
  CKXOR2D1 U41 ( .A1(n87), .A2(n161), .Z(n36) );
  CKND2 U42 ( .I(n141), .ZN(n91) );
  INVD4 U43 ( .I(n99), .ZN(n141) );
  MUX2D1 U44 ( .I0(n118), .I1(n117), .S(n94), .Z(n123) );
  BUFFD2 U45 ( .I(n93), .Z(n92) );
  IND2D4 U46 ( .A1(n99), .B1(n94), .ZN(n155) );
  OR2XD1 U47 ( .A1(n96), .A2(n97), .Z(n37) );
  AOI31D2 U48 ( .A1(n125), .A2(n146), .A3(n124), .B(n123), .ZN(n96) );
  INVD2 U49 ( .I(n156), .ZN(n142) );
  CKND2D2 U50 ( .A1(n110), .A2(n109), .ZN(n160) );
  CKND8 U51 ( .I(n109), .ZN(n120) );
  ND2D2 U52 ( .A1(n104), .A2(n105), .ZN(n106) );
  NR2D2 U53 ( .A1(n150), .A2(n149), .ZN(n158) );
  AN2D4 U54 ( .A1(n103), .A2(n152), .Z(n110) );
  INVD1 U55 ( .I(n168), .ZN(n145) );
  INVD6 U56 ( .I(n6), .ZN(n109) );
  CKND2D0 U57 ( .A1(n6), .A2(n98), .ZN(n133) );
  MUX2D2 U58 ( .I0(n115), .I1(n162), .S(n116), .Z(n157) );
  ND3D2 U59 ( .A1(n138), .A2(n136), .A3(n137), .ZN(n35) );
  AOI32D2 U60 ( .A1(n90), .A2(n135), .A3(n130), .B1(n83), .B2(n134), .ZN(n136)
         );
  AOI21D1 U61 ( .A1(n92), .A2(n141), .B(n103), .ZN(n135) );
  AN4D4 U62 ( .A1(n100), .A2(n93), .A3(n141), .A4(n131), .Z(n119) );
  CKND2D1 U63 ( .A1(n106), .A2(n85), .ZN(n125) );
  ND3D1 U64 ( .A1(n152), .A2(n10), .A3(n109), .ZN(n153) );
  ND2D1 U65 ( .A1(n151), .A2(n109), .ZN(n101) );
  INVD1 U66 ( .I(n101), .ZN(n102) );
  AOI32D1 U67 ( .A1(n151), .A2(n109), .A3(n152), .B1(n111), .B2(n95), .ZN(n129) );
  INVD1 U68 ( .I(n120), .ZN(n105) );
  CKND2D2 U69 ( .A1(n132), .A2(n90), .ZN(n137) );
  OR2XD1 U70 ( .A1(n156), .A2(n155), .Z(n116) );
  IND2D2 U71 ( .A1(n155), .B1(n142), .ZN(n147) );
  ND2D2 U72 ( .A1(n145), .A2(n120), .ZN(n164) );
  ND3D1 U73 ( .A1(n109), .A2(n152), .A3(n151), .ZN(n163) );
  IND3D2 U74 ( .A1(n130), .B1(n129), .B2(n128), .ZN(n138) );
  IAO21D1 U75 ( .A1(n94), .A2(n121), .B(n163), .ZN(n132) );
  CKND1 U76 ( .I(n133), .ZN(n130) );
  CKND1 U77 ( .I(n161), .ZN(n126) );
  AN2XD1 U78 ( .A1(n154), .A2(n153), .Z(n115) );
  INVD1 U79 ( .I(clr), .ZN(n167) );
  INVD3 U80 ( .I(counter_1_), .ZN(n131) );
  NR3D1 U81 ( .A1(n84), .A2(n127), .A3(n113), .ZN(n128) );
  CKAN2D1 U82 ( .A1(n99), .A2(counter_1_), .Z(n118) );
  INVD6 U83 ( .I(n10), .ZN(n151) );
  OAI211D0 U84 ( .A1(n111), .A2(n120), .B(n162), .C(n161), .ZN(lightREW) );
  CKND1 U85 ( .I(n141), .ZN(n121) );
  INVD6 U86 ( .I(n98), .ZN(n152) );
  OAI221D1 U87 ( .A1(n147), .A2(n164), .B1(n119), .B2(n159), .C(n146), .ZN(
        n165) );
  CKND1 U88 ( .I(n163), .ZN(lightGNS) );
  NR2XD1 U89 ( .A1(n148), .A2(n163), .ZN(n149) );
  OAI21D1 U90 ( .A1(n121), .A2(n113), .B(n104), .ZN(n124) );
endmodule


module traffic_lights_top ( clk, clr, RNS, YNS, GNS, REW, YEW, GEW );
  input clk, clr;
  output RNS, YNS, GNS, REW, YEW, GEW;


  traffic_lights_ct_notblocking U1 ( .clk(clk), .clr(clr), .lightRNS(RNS), 
        .lightYNS(YNS), .lightGNS(GNS), .lightREW(REW), .lightYEW(YEW), 
        .lightGEW(GEW) );
endmodule

