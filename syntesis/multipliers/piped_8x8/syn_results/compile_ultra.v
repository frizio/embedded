
module mult_piped_8x8_2sC_DW_mult_uns_0 ( a, b, product );
  input [7:0] a;
  input [7:0] b;
  output [15:0] product;
  wire   n2, n3, n4, n5, n6, n7, n8, n9, n10, n11, n12, n13, n14, n15, n16,
         n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
         n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
         n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58,
         n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72,
         n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n86, n87,
         n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
         n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
         n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
         n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133,
         n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
         n145, n146, n147, n215, n216, n217, n218, n219, n220, n221, n222,
         n223, n224, n225, n226, n227, n228, n229, n230, n231, n232, n233,
         n234, n235, n236, n237, n238, n239, n240, n241, n242, n243, n244,
         n245;

  FA1D0 U126 ( .A(n40), .B(n33), .CI(n7), .CO(n6), .S(product[9]) );
  FA1D0 U127 ( .A(n222), .B(n49), .CI(n44), .CO(n40), .S(n41) );
  FA1D0 U128 ( .A(n32), .B(n27), .CI(n6), .CO(n5), .S(product[10]) );
  FA1D0 U129 ( .A(n26), .B(n22), .CI(n5), .CO(n4), .S(product[11]) );
  FA1D0 U130 ( .A(n21), .B(n19), .CI(n4), .CO(n3), .S(product[12]) );
  FA1D0 U131 ( .A(n69), .B(n75), .CI(n11), .CO(n10), .S(product[5]) );
  FA1D0 U132 ( .A(n61), .B(n68), .CI(n10), .CO(n9), .S(product[6]) );
  FA1D0 U133 ( .A(n51), .B(n60), .CI(n9), .CO(n8), .S(product[7]) );
  FA1D0 U134 ( .A(n41), .B(n50), .CI(n8), .CO(n7), .S(product[8]) );
  FA1D0 U135 ( .A(n220), .B(n39), .CI(n43), .CO(n32), .S(n33) );
  FA1D0 U136 ( .A(n229), .B(n78), .CI(n81), .CO(n75), .S(n76) );
  FA1D0 U137 ( .A(n228), .B(n71), .CI(n74), .CO(n68), .S(n69) );
  FA1D0 U138 ( .A(n62), .B(n58), .CI(n59), .CO(n49), .S(n224) );
  FA1D0 U139 ( .A(n226), .B(n64), .CI(n67), .CO(n60), .S(n61) );
  FA1D0 U140 ( .A(n55), .B(n52), .CI(n53), .CO(n39), .S(n222) );
  FA1D0 U141 ( .A(n224), .B(n63), .CI(n54), .CO(n50), .S(n51) );
  FA1D0 U142 ( .A(n42), .B(n45), .CI(n36), .CO(n31), .S(n220) );
  FA1D0 U143 ( .A(n223), .B(n46), .CI(n48), .CO(n43), .S(n44) );
  FA1D0 U144 ( .A(n221), .B(n38), .CI(n47), .CO(n35), .S(n36) );
  FA1D0 U145 ( .A(n80), .B(n82), .CI(n13), .CO(n12), .S(product[3]) );
  FA1D0 U146 ( .A(n76), .B(n79), .CI(n12), .CO(n11), .S(product[4]) );
  FA1D0 U147 ( .A(n225), .B(n56), .CI(n65), .CO(n53), .S(n54) );
  FA1D0 U148 ( .A(n218), .B(n31), .CI(n35), .CO(n26), .S(n27) );
  FA1D0 U149 ( .A(n217), .B(n25), .CI(n29), .CO(n21), .S(n22) );
  FA1D0 U150 ( .A(n216), .B(n20), .CI(n23), .CO(n18), .S(n19) );
  FA1D0 U151 ( .A(n18), .B(n16), .CI(n3), .CO(n2), .S(product[13]) );
  NR2D1 U152 ( .A1(n237), .A2(n245), .ZN(product[0]) );
  XOR3D1 U153 ( .A1(n2), .A2(n15), .A3(n215), .Z(product[14]) );
  NR2D1 U154 ( .A1(n230), .A2(n238), .ZN(n215) );
  HA1D0 U155 ( .A(n100), .B(n107), .CO(n65), .S(n66) );
  NR2D1 U156 ( .A1(n245), .A2(n231), .ZN(n100) );
  NR2D1 U157 ( .A1(n241), .A2(n236), .ZN(n107) );
  FA1D0 U158 ( .A(n130), .B(n144), .CI(n137), .CO(n74), .S(n229) );
  NR2D1 U159 ( .A1(n237), .A2(n240), .ZN(n144) );
  NR2D1 U160 ( .A1(n243), .A2(n233), .ZN(n137) );
  NR2D1 U161 ( .A1(n242), .A2(n234), .ZN(n130) );
  FA1D0 U162 ( .A(n138), .B(n145), .CI(n83), .CO(n79), .S(n80) );
  NR2D1 U163 ( .A1(n237), .A2(n243), .ZN(n145) );
  NR2D1 U164 ( .A1(n242), .A2(n233), .ZN(n138) );
  FA1D0 U165 ( .A(n114), .B(n142), .CI(n135), .CO(n62), .S(n227) );
  NR2D1 U166 ( .A1(n237), .A2(n239), .ZN(n142) );
  NR2D1 U167 ( .A1(n244), .A2(n233), .ZN(n135) );
  NR2D1 U168 ( .A1(n242), .A2(n232), .ZN(n114) );
  FA1D0 U169 ( .A(n120), .B(n134), .CI(n127), .CO(n52), .S(n225) );
  NR2D1 U170 ( .A1(n239), .A2(n233), .ZN(n134) );
  NR2D1 U171 ( .A1(n244), .A2(n234), .ZN(n127) );
  NR2D1 U172 ( .A1(n240), .A2(n235), .ZN(n120) );
  FA1D0 U173 ( .A(n105), .B(n119), .CI(n57), .CO(n42), .S(n223) );
  NR2D1 U174 ( .A1(n244), .A2(n235), .ZN(n119) );
  NR2D1 U175 ( .A1(n243), .A2(n236), .ZN(n105) );
  FA1D0 U176 ( .A(n118), .B(n104), .CI(n111), .CO(n34), .S(n221) );
  NR2D1 U177 ( .A1(n240), .A2(n236), .ZN(n104) );
  NR2D1 U178 ( .A1(n244), .A2(n232), .ZN(n111) );
  NR2D1 U179 ( .A1(n239), .A2(n235), .ZN(n118) );
  FA1D0 U180 ( .A(n89), .B(n103), .CI(n117), .CO(n28), .S(n219) );
  NR2D1 U181 ( .A1(n244), .A2(n236), .ZN(n103) );
  NR2D1 U182 ( .A1(n238), .A2(n235), .ZN(n117) );
  NR2D1 U183 ( .A1(n230), .A2(n243), .ZN(n89) );
  FA1D0 U184 ( .A(n87), .B(n101), .CI(n94), .CO(n17), .S(n216) );
  NR2D1 U185 ( .A1(n238), .A2(n236), .ZN(n101) );
  NR2D1 U186 ( .A1(n231), .A2(n239), .ZN(n94) );
  NR2D1 U187 ( .A1(n230), .A2(n244), .ZN(n87) );
  FA1D0 U188 ( .A(n34), .B(n96), .CI(n30), .CO(n25), .S(n218) );
  NR2D1 U189 ( .A1(n231), .A2(n240), .ZN(n96) );
  FA1D0 U190 ( .A(n28), .B(n95), .CI(n24), .CO(n20), .S(n217) );
  NR2D1 U191 ( .A1(n231), .A2(n244), .ZN(n95) );
  FA1D0 U192 ( .A(n77), .B(n136), .CI(n73), .CO(n67), .S(n228) );
  NR2D1 U193 ( .A1(n240), .A2(n233), .ZN(n136) );
  FA1D0 U194 ( .A(n102), .B(n109), .CI(n88), .CO(n23), .S(n24) );
  NR2D1 U195 ( .A1(n238), .A2(n232), .ZN(n109) );
  NR2D1 U196 ( .A1(n230), .A2(n240), .ZN(n88) );
  NR2D1 U197 ( .A1(n239), .A2(n236), .ZN(n102) );
  FA1D0 U198 ( .A(n219), .B(n37), .CI(n110), .CO(n29), .S(n30) );
  NR2D1 U199 ( .A1(n239), .A2(n232), .ZN(n110) );
  FA1D0 U200 ( .A(n14), .B(n132), .CI(n84), .CO(n13), .S(product[2]) );
  NR2D1 U201 ( .A1(n245), .A2(n234), .ZN(n132) );
  FA1D0 U202 ( .A(n72), .B(n128), .CI(n66), .CO(n59), .S(n226) );
  NR2D1 U203 ( .A1(n240), .A2(n234), .ZN(n128) );
  FA1D0 U204 ( .A(n106), .B(n141), .CI(n113), .CO(n55), .S(n56) );
  NR2D1 U205 ( .A1(n238), .A2(n237), .ZN(n141) );
  NR2D1 U206 ( .A1(n243), .A2(n232), .ZN(n113) );
  NR2D1 U207 ( .A1(n242), .A2(n236), .ZN(n106) );
  FA1D0 U208 ( .A(n133), .B(n112), .CI(n126), .CO(n45), .S(n46) );
  NR2D1 U209 ( .A1(n240), .A2(n232), .ZN(n112) );
  NR2D1 U210 ( .A1(n239), .A2(n234), .ZN(n126) );
  NR2D1 U211 ( .A1(n238), .A2(n233), .ZN(n133) );
  FA1D0 U212 ( .A(n90), .B(n125), .CI(n97), .CO(n37), .S(n38) );
  NR2D1 U213 ( .A1(n238), .A2(n234), .ZN(n125) );
  NR2D1 U214 ( .A1(n231), .A2(n243), .ZN(n97) );
  NR2D1 U215 ( .A1(n230), .A2(n242), .ZN(n90) );
  FA1D0 U216 ( .A(n86), .B(n93), .CI(n17), .CO(n15), .S(n16) );
  NR2D1 U217 ( .A1(n238), .A2(n231), .ZN(n93) );
  NR2D1 U218 ( .A1(n230), .A2(n239), .ZN(n86) );
  FA1D0 U219 ( .A(n227), .B(n70), .CI(n121), .CO(n63), .S(n64) );
  NR2D1 U220 ( .A1(n243), .A2(n235), .ZN(n121) );
  FA1D0 U221 ( .A(n122), .B(n143), .CI(n129), .CO(n70), .S(n71) );
  NR2D1 U222 ( .A1(n237), .A2(n244), .ZN(n143) );
  NR2D1 U223 ( .A1(n243), .A2(n234), .ZN(n129) );
  NR2D1 U224 ( .A1(n242), .A2(n235), .ZN(n122) );
  HA1D0 U225 ( .A(n108), .B(n115), .CO(n72), .S(n73) );
  NR2D1 U226 ( .A1(n245), .A2(n236), .ZN(n108) );
  NR2D1 U227 ( .A1(n241), .A2(n232), .ZN(n115) );
  HA1D0 U228 ( .A(n92), .B(n99), .CO(n57), .S(n58) );
  NR2D1 U229 ( .A1(n230), .A2(n245), .ZN(n92) );
  NR2D1 U230 ( .A1(n231), .A2(n241), .ZN(n99) );
  HA1D0 U231 ( .A(n147), .B(n140), .CO(n14), .S(product[1]) );
  NR2D1 U232 ( .A1(n237), .A2(n241), .ZN(n147) );
  NR2D1 U233 ( .A1(n245), .A2(n233), .ZN(n140) );
  HA1D0 U234 ( .A(n116), .B(n123), .CO(n77), .S(n78) );
  NR2D1 U235 ( .A1(n245), .A2(n232), .ZN(n116) );
  NR2D1 U236 ( .A1(n241), .A2(n235), .ZN(n123) );
  HA1D0 U237 ( .A(n139), .B(n146), .CO(n83), .S(n84) );
  NR2D1 U238 ( .A1(n241), .A2(n233), .ZN(n139) );
  NR2D1 U239 ( .A1(n237), .A2(n242), .ZN(n146) );
  HA1D0 U240 ( .A(n124), .B(n131), .CO(n81), .S(n82) );
  NR2D1 U241 ( .A1(n245), .A2(n235), .ZN(n124) );
  NR2D1 U242 ( .A1(n241), .A2(n234), .ZN(n131) );
  HA1D0 U243 ( .A(n91), .B(n98), .CO(n47), .S(n48) );
  NR2D1 U244 ( .A1(n230), .A2(n241), .ZN(n91) );
  NR2D1 U245 ( .A1(n231), .A2(n242), .ZN(n98) );
  INVD1 U246 ( .I(b[1]), .ZN(n241) );
  INVD1 U247 ( .I(a[1]), .ZN(n233) );
  INVD1 U248 ( .I(a[2]), .ZN(n234) );
  INVD1 U249 ( .I(b[2]), .ZN(n242) );
  INVD1 U250 ( .I(b[3]), .ZN(n243) );
  INVD1 U251 ( .I(a[3]), .ZN(n235) );
  INVD1 U252 ( .I(b[4]), .ZN(n240) );
  INVD1 U253 ( .I(a[4]), .ZN(n232) );
  INVD1 U254 ( .I(b[5]), .ZN(n244) );
  INVD1 U255 ( .I(a[5]), .ZN(n236) );
  INVD1 U256 ( .I(a[7]), .ZN(n230) );
  INVD1 U257 ( .I(b[7]), .ZN(n238) );
  INVD1 U258 ( .I(b[0]), .ZN(n245) );
  INVD1 U259 ( .I(a[0]), .ZN(n237) );
  INVD1 U260 ( .I(b[6]), .ZN(n239) );
  INVD1 U261 ( .I(a[6]), .ZN(n231) );
endmodule


module mult_piped_8x8_2sC ( a, b, clk, reset, y );
  input [7:0] a;
  input [7:0] b;
  output [15:0] y;
  input clk, reset;
  wire   yR_6__15_, yR_6__14_, yR_6__13_, yR_6__12_, yR_6__11_, yR_6__10_,
         yR_6__9_, yR_6__8_, yR_6__7_, yR_6__6_, yR_6__5_, yR_6__4_, yR_6__3_,
         yR_6__2_, yR_6__1_, yR_6__0_, yR_5__15_, yR_5__14_, yR_5__13_,
         yR_5__12_, yR_5__11_, yR_5__10_, yR_5__9_, yR_5__8_, yR_5__7_,
         yR_5__6_, yR_5__5_, yR_5__4_, yR_5__3_, yR_5__2_, yR_5__1_, yR_5__0_,
         yR_4__15_, yR_4__14_, yR_4__13_, yR_4__12_, yR_4__11_, yR_4__10_,
         yR_4__9_, yR_4__8_, yR_4__7_, yR_4__6_, yR_4__5_, yR_4__4_, yR_4__3_,
         yR_4__2_, yR_4__1_, yR_4__0_, yR_3__15_, yR_3__14_, yR_3__13_,
         yR_3__12_, yR_3__11_, yR_3__10_, yR_3__9_, yR_3__8_, yR_3__7_,
         yR_3__6_, yR_3__5_, yR_3__4_, yR_3__3_, yR_3__2_, yR_3__1_, yR_3__0_,
         yR_2__15_, yR_2__14_, yR_2__13_, yR_2__12_, yR_2__11_, yR_2__10_,
         yR_2__9_, yR_2__8_, yR_2__7_, yR_2__6_, yR_2__5_, yR_2__4_, yR_2__3_,
         yR_2__2_, yR_2__1_, yR_2__0_, yR_1__15_, yR_1__14_, yR_1__13_,
         yR_1__12_, yR_1__11_, yR_1__10_, yR_1__9_, yR_1__8_, yR_1__7_,
         yR_1__6_, yR_1__5_, yR_1__4_, yR_1__3_, yR_1__2_, yR_1__1_, yR_1__0_,
         yR_0__15_, yR_0__14_, yR_0__13_, yR_0__12_, yR_0__11_, yR_0__10_,
         yR_0__9_, yR_0__8_, yR_0__7_, yR_0__6_, yR_0__5_, yR_0__4_, yR_0__3_,
         yR_0__2_, yR_0__1_, yR_0__0_, N15, N16, N17, N18, N19, N20, N21, N32,
         N33, N34, N35, N36, N37, N38, N40, N41, N42, N43, N44, N45, N46, N47,
         N48, N49, N50, N51, N52, N53, N57, n49, n51, n52, n55, n56, n57, n58,
         n59, n60, n61, n62, n63, n64, n65, n66, n133, n134, n135, n136, n137,
         n138, n139, n140, n141, n142, n143, n144, n145, n146, n147, n148,
         n149, n150, n151, n152, n153, n154, n155, n156, n157, n158, n159,
         n160, n161, n162, n163, n164, n165, n166, n167, n168, n169, n170,
         n171, n172, n173, n174, n175, n176, n177, n178, n179, n180, n181,
         n182, n183, n184, SYNOPSYS_UNCONNECTED_1;

  MOAI22D1 U62 ( .A1(n133), .A2(n183), .B1(n183), .B2(N53), .ZN(n66) );
  XNR2D1 U64 ( .A1(n135), .A2(N40), .ZN(n65) );
  CKXOR2D1 U66 ( .A1(N51), .A2(n136), .Z(n64) );
  XNR2D1 U68 ( .A1(n138), .A2(N50), .ZN(n63) );
  XNR2D1 U70 ( .A1(n140), .A2(N48), .ZN(n62) );
  CKXOR2D1 U72 ( .A1(N47), .A2(n142), .Z(n61) );
  XNR2D1 U74 ( .A1(n144), .A2(N46), .ZN(n60) );
  XNR2D1 U76 ( .A1(n146), .A2(N44), .ZN(n59) );
  XNR2D1 U78 ( .A1(n148), .A2(N43), .ZN(n58) );
  XNR2D1 U80 ( .A1(n150), .A2(N41), .ZN(n57) );
  CKXOR2D1 U82 ( .A1(N49), .A2(n152), .Z(n56) );
  CKXOR2D1 U84 ( .A1(N45), .A2(n154), .Z(n55) );
  XNR2D1 U86 ( .A1(n156), .A2(N52), .ZN(n52) );
  XNR2D1 U88 ( .A1(n157), .A2(N42), .ZN(n51) );
  OR2D1 U100 ( .A1(n149), .A2(N43), .Z(n147) );
  OR2D1 U101 ( .A1(n158), .A2(N42), .Z(n149) );
  OR2D1 U102 ( .A1(n151), .A2(N41), .Z(n158) );
  OR2D1 U103 ( .A1(N57), .A2(N40), .Z(n151) );
  CKXOR2D1 U105 ( .A1(b[6]), .A2(n160), .Z(N37) );
  XNR2D1 U108 ( .A1(n162), .A2(b[5]), .ZN(N36) );
  CKXOR2D1 U111 ( .A1(b[4]), .A2(n164), .Z(N35) );
  XNR2D1 U114 ( .A1(n166), .A2(b[3]), .ZN(N34) );
  OR2D1 U116 ( .A1(n167), .A2(b[2]), .Z(n165) );
  XNR2D1 U117 ( .A1(n168), .A2(b[2]), .ZN(N33) );
  OR2D1 U119 ( .A1(b[1]), .A2(b[0]), .Z(n167) );
  XNR2D1 U120 ( .A1(n169), .A2(b[1]), .ZN(N32) );
  CKXOR2D1 U123 ( .A1(a[6]), .A2(n171), .Z(N20) );
  XNR2D1 U126 ( .A1(n173), .A2(a[5]), .ZN(N19) );
  CKXOR2D1 U129 ( .A1(a[4]), .A2(n175), .Z(N18) );
  XNR2D1 U132 ( .A1(n177), .A2(a[3]), .ZN(N17) );
  OR2D1 U134 ( .A1(n178), .A2(a[2]), .Z(n176) );
  XNR2D1 U135 ( .A1(n179), .A2(a[2]), .ZN(N16) );
  OR2D1 U137 ( .A1(a[1]), .A2(a[0]), .Z(n178) );
  XNR2D1 U138 ( .A1(n180), .A2(a[1]), .ZN(N15) );
  mult_piped_8x8_2sC_DW_mult_uns_0 mult_67_C50 ( .a({N21, N20, N19, N18, N17, 
        N16, N15, a[0]}), .b({N38, N37, N36, N35, N34, N33, N32, b[0]}), 
        .product({SYNOPSYS_UNCONNECTED_1, N53, N52, N51, N50, N49, N48, N47, 
        N46, N45, N44, N43, N42, N41, N40, N57}) );
  DFKCNQD1 yR_reg_0__15_ ( .CN(n49), .D(n181), .CP(clk), .Q(yR_0__15_) );
  DFKCNQD1 yR_reg_0__0_ ( .CN(n49), .D(N57), .CP(clk), .Q(yR_0__0_) );
  DFKCNQD1 yR_reg_0__1_ ( .CN(n49), .D(n65), .CP(clk), .Q(yR_0__1_) );
  DFKCNQD1 yR_reg_0__2_ ( .CN(n49), .D(n57), .CP(clk), .Q(yR_0__2_) );
  DFKCNQD1 yR_reg_0__3_ ( .CN(n49), .D(n51), .CP(clk), .Q(yR_0__3_) );
  DFKCNQD1 yR_reg_0__4_ ( .CN(n49), .D(n58), .CP(clk), .Q(yR_0__4_) );
  DFKCNQD1 yR_reg_0__5_ ( .CN(n49), .D(n59), .CP(clk), .Q(yR_0__5_) );
  DFKCNQD1 yR_reg_0__7_ ( .CN(n49), .D(n60), .CP(clk), .Q(yR_0__7_) );
  DFKCNQD1 yR_reg_0__9_ ( .CN(n49), .D(n62), .CP(clk), .Q(yR_0__9_) );
  DFKCNQD1 yR_reg_0__11_ ( .CN(n49), .D(n63), .CP(clk), .Q(yR_0__11_) );
  DFKCNQD1 yR_reg_0__13_ ( .CN(n49), .D(n52), .CP(clk), .Q(yR_0__13_) );
  DFKCNQD1 yR_reg_0__6_ ( .CN(n49), .D(n55), .CP(clk), .Q(yR_0__6_) );
  DFKCNQD1 yR_reg_0__8_ ( .CN(n49), .D(n61), .CP(clk), .Q(yR_0__8_) );
  DFKCNQD1 yR_reg_0__10_ ( .CN(n49), .D(n56), .CP(clk), .Q(yR_0__10_) );
  DFKCNQD1 yR_reg_0__12_ ( .CN(n49), .D(n64), .CP(clk), .Q(yR_0__12_) );
  DFKCNQD1 yR_reg_0__14_ ( .CN(n49), .D(n66), .CP(clk), .Q(yR_0__14_) );
  DFQD1 yR_reg_6__15_ ( .D(yR_5__15_), .CP(clk), .Q(yR_6__15_) );
  DFQD1 yR_reg_6__14_ ( .D(yR_5__14_), .CP(clk), .Q(yR_6__14_) );
  DFQD1 yR_reg_6__13_ ( .D(yR_5__13_), .CP(clk), .Q(yR_6__13_) );
  DFQD1 yR_reg_6__12_ ( .D(yR_5__12_), .CP(clk), .Q(yR_6__12_) );
  DFQD1 yR_reg_6__11_ ( .D(yR_5__11_), .CP(clk), .Q(yR_6__11_) );
  DFQD1 yR_reg_6__10_ ( .D(yR_5__10_), .CP(clk), .Q(yR_6__10_) );
  DFQD1 yR_reg_6__9_ ( .D(yR_5__9_), .CP(clk), .Q(yR_6__9_) );
  DFQD1 yR_reg_6__8_ ( .D(yR_5__8_), .CP(clk), .Q(yR_6__8_) );
  DFQD1 yR_reg_6__7_ ( .D(yR_5__7_), .CP(clk), .Q(yR_6__7_) );
  DFQD1 yR_reg_6__6_ ( .D(yR_5__6_), .CP(clk), .Q(yR_6__6_) );
  DFQD1 yR_reg_6__5_ ( .D(yR_5__5_), .CP(clk), .Q(yR_6__5_) );
  DFQD1 yR_reg_6__4_ ( .D(yR_5__4_), .CP(clk), .Q(yR_6__4_) );
  DFQD1 yR_reg_6__3_ ( .D(yR_5__3_), .CP(clk), .Q(yR_6__3_) );
  DFQD1 yR_reg_6__2_ ( .D(yR_5__2_), .CP(clk), .Q(yR_6__2_) );
  DFQD1 yR_reg_6__1_ ( .D(yR_5__1_), .CP(clk), .Q(yR_6__1_) );
  DFQD1 yR_reg_6__0_ ( .D(yR_5__0_), .CP(clk), .Q(yR_6__0_) );
  DFQD1 yR_reg_5__15_ ( .D(yR_4__15_), .CP(clk), .Q(yR_5__15_) );
  DFQD1 yR_reg_5__14_ ( .D(yR_4__14_), .CP(clk), .Q(yR_5__14_) );
  DFQD1 yR_reg_5__13_ ( .D(yR_4__13_), .CP(clk), .Q(yR_5__13_) );
  DFQD1 yR_reg_5__12_ ( .D(yR_4__12_), .CP(clk), .Q(yR_5__12_) );
  DFQD1 yR_reg_5__11_ ( .D(yR_4__11_), .CP(clk), .Q(yR_5__11_) );
  DFQD1 yR_reg_5__10_ ( .D(yR_4__10_), .CP(clk), .Q(yR_5__10_) );
  DFQD1 yR_reg_5__9_ ( .D(yR_4__9_), .CP(clk), .Q(yR_5__9_) );
  DFQD1 yR_reg_5__8_ ( .D(yR_4__8_), .CP(clk), .Q(yR_5__8_) );
  DFQD1 yR_reg_5__7_ ( .D(yR_4__7_), .CP(clk), .Q(yR_5__7_) );
  DFQD1 yR_reg_5__6_ ( .D(yR_4__6_), .CP(clk), .Q(yR_5__6_) );
  DFQD1 yR_reg_5__5_ ( .D(yR_4__5_), .CP(clk), .Q(yR_5__5_) );
  DFQD1 yR_reg_5__4_ ( .D(yR_4__4_), .CP(clk), .Q(yR_5__4_) );
  DFQD1 yR_reg_5__3_ ( .D(yR_4__3_), .CP(clk), .Q(yR_5__3_) );
  DFQD1 yR_reg_5__2_ ( .D(yR_4__2_), .CP(clk), .Q(yR_5__2_) );
  DFQD1 yR_reg_5__1_ ( .D(yR_4__1_), .CP(clk), .Q(yR_5__1_) );
  DFQD1 yR_reg_5__0_ ( .D(yR_4__0_), .CP(clk), .Q(yR_5__0_) );
  DFQD1 yR_reg_4__15_ ( .D(yR_3__15_), .CP(clk), .Q(yR_4__15_) );
  DFQD1 yR_reg_4__14_ ( .D(yR_3__14_), .CP(clk), .Q(yR_4__14_) );
  DFQD1 yR_reg_4__13_ ( .D(yR_3__13_), .CP(clk), .Q(yR_4__13_) );
  DFQD1 yR_reg_4__12_ ( .D(yR_3__12_), .CP(clk), .Q(yR_4__12_) );
  DFQD1 yR_reg_4__11_ ( .D(yR_3__11_), .CP(clk), .Q(yR_4__11_) );
  DFQD1 yR_reg_4__10_ ( .D(yR_3__10_), .CP(clk), .Q(yR_4__10_) );
  DFQD1 yR_reg_4__9_ ( .D(yR_3__9_), .CP(clk), .Q(yR_4__9_) );
  DFQD1 yR_reg_4__8_ ( .D(yR_3__8_), .CP(clk), .Q(yR_4__8_) );
  DFQD1 yR_reg_4__7_ ( .D(yR_3__7_), .CP(clk), .Q(yR_4__7_) );
  DFQD1 yR_reg_4__6_ ( .D(yR_3__6_), .CP(clk), .Q(yR_4__6_) );
  DFQD1 yR_reg_4__5_ ( .D(yR_3__5_), .CP(clk), .Q(yR_4__5_) );
  DFQD1 yR_reg_4__4_ ( .D(yR_3__4_), .CP(clk), .Q(yR_4__4_) );
  DFQD1 yR_reg_4__3_ ( .D(yR_3__3_), .CP(clk), .Q(yR_4__3_) );
  DFQD1 yR_reg_4__2_ ( .D(yR_3__2_), .CP(clk), .Q(yR_4__2_) );
  DFQD1 yR_reg_4__1_ ( .D(yR_3__1_), .CP(clk), .Q(yR_4__1_) );
  DFQD1 yR_reg_4__0_ ( .D(yR_3__0_), .CP(clk), .Q(yR_4__0_) );
  DFQD1 yR_reg_3__0_ ( .D(yR_2__0_), .CP(clk), .Q(yR_3__0_) );
  DFQD1 yR_reg_2__0_ ( .D(yR_1__0_), .CP(clk), .Q(yR_2__0_) );
  DFQD1 yR_reg_1__0_ ( .D(yR_0__0_), .CP(clk), .Q(yR_1__0_) );
  DFQD1 yR_reg_3__1_ ( .D(yR_2__1_), .CP(clk), .Q(yR_3__1_) );
  DFQD1 yR_reg_2__1_ ( .D(yR_1__1_), .CP(clk), .Q(yR_2__1_) );
  DFQD1 yR_reg_1__1_ ( .D(yR_0__1_), .CP(clk), .Q(yR_1__1_) );
  DFQD1 yR_reg_3__2_ ( .D(yR_2__2_), .CP(clk), .Q(yR_3__2_) );
  DFQD1 yR_reg_2__2_ ( .D(yR_1__2_), .CP(clk), .Q(yR_2__2_) );
  DFQD1 yR_reg_1__2_ ( .D(yR_0__2_), .CP(clk), .Q(yR_1__2_) );
  DFQD1 yR_reg_3__3_ ( .D(yR_2__3_), .CP(clk), .Q(yR_3__3_) );
  DFQD1 yR_reg_2__3_ ( .D(yR_1__3_), .CP(clk), .Q(yR_2__3_) );
  DFQD1 yR_reg_1__3_ ( .D(yR_0__3_), .CP(clk), .Q(yR_1__3_) );
  DFQD1 yR_reg_3__4_ ( .D(yR_2__4_), .CP(clk), .Q(yR_3__4_) );
  DFQD1 yR_reg_2__4_ ( .D(yR_1__4_), .CP(clk), .Q(yR_2__4_) );
  DFQD1 yR_reg_1__4_ ( .D(yR_0__4_), .CP(clk), .Q(yR_1__4_) );
  DFQD1 yR_reg_3__5_ ( .D(yR_2__5_), .CP(clk), .Q(yR_3__5_) );
  DFQD1 yR_reg_2__5_ ( .D(yR_1__5_), .CP(clk), .Q(yR_2__5_) );
  DFQD1 yR_reg_1__5_ ( .D(yR_0__5_), .CP(clk), .Q(yR_1__5_) );
  DFQD1 yR_reg_3__6_ ( .D(yR_2__6_), .CP(clk), .Q(yR_3__6_) );
  DFQD1 yR_reg_2__6_ ( .D(yR_1__6_), .CP(clk), .Q(yR_2__6_) );
  DFQD1 yR_reg_1__6_ ( .D(yR_0__6_), .CP(clk), .Q(yR_1__6_) );
  DFQD1 yR_reg_3__7_ ( .D(yR_2__7_), .CP(clk), .Q(yR_3__7_) );
  DFQD1 yR_reg_2__7_ ( .D(yR_1__7_), .CP(clk), .Q(yR_2__7_) );
  DFQD1 yR_reg_1__7_ ( .D(yR_0__7_), .CP(clk), .Q(yR_1__7_) );
  DFQD1 yR_reg_3__8_ ( .D(yR_2__8_), .CP(clk), .Q(yR_3__8_) );
  DFQD1 yR_reg_2__8_ ( .D(yR_1__8_), .CP(clk), .Q(yR_2__8_) );
  DFQD1 yR_reg_1__8_ ( .D(yR_0__8_), .CP(clk), .Q(yR_1__8_) );
  DFQD1 yR_reg_3__9_ ( .D(yR_2__9_), .CP(clk), .Q(yR_3__9_) );
  DFQD1 yR_reg_2__9_ ( .D(yR_1__9_), .CP(clk), .Q(yR_2__9_) );
  DFQD1 yR_reg_1__9_ ( .D(yR_0__9_), .CP(clk), .Q(yR_1__9_) );
  DFQD1 yR_reg_3__10_ ( .D(yR_2__10_), .CP(clk), .Q(yR_3__10_) );
  DFQD1 yR_reg_2__10_ ( .D(yR_1__10_), .CP(clk), .Q(yR_2__10_) );
  DFQD1 yR_reg_1__10_ ( .D(yR_0__10_), .CP(clk), .Q(yR_1__10_) );
  DFQD1 yR_reg_3__11_ ( .D(yR_2__11_), .CP(clk), .Q(yR_3__11_) );
  DFQD1 yR_reg_2__11_ ( .D(yR_1__11_), .CP(clk), .Q(yR_2__11_) );
  DFQD1 yR_reg_1__11_ ( .D(yR_0__11_), .CP(clk), .Q(yR_1__11_) );
  DFQD1 yR_reg_3__12_ ( .D(yR_2__12_), .CP(clk), .Q(yR_3__12_) );
  DFQD1 yR_reg_2__12_ ( .D(yR_1__12_), .CP(clk), .Q(yR_2__12_) );
  DFQD1 yR_reg_1__12_ ( .D(yR_0__12_), .CP(clk), .Q(yR_1__12_) );
  DFQD1 yR_reg_3__13_ ( .D(yR_2__13_), .CP(clk), .Q(yR_3__13_) );
  DFQD1 yR_reg_2__13_ ( .D(yR_1__13_), .CP(clk), .Q(yR_2__13_) );
  DFQD1 yR_reg_1__13_ ( .D(yR_0__13_), .CP(clk), .Q(yR_1__13_) );
  DFQD1 yR_reg_3__14_ ( .D(yR_2__14_), .CP(clk), .Q(yR_3__14_) );
  DFQD1 yR_reg_2__14_ ( .D(yR_1__14_), .CP(clk), .Q(yR_2__14_) );
  DFQD1 yR_reg_1__14_ ( .D(yR_0__14_), .CP(clk), .Q(yR_1__14_) );
  DFQD1 yR_reg_3__15_ ( .D(yR_2__15_), .CP(clk), .Q(yR_3__15_) );
  DFQD1 yR_reg_2__15_ ( .D(yR_1__15_), .CP(clk), .Q(yR_2__15_) );
  DFQD1 yR_reg_1__15_ ( .D(yR_0__15_), .CP(clk), .Q(yR_1__15_) );
  DFQD1 yR_reg_7__15_ ( .D(yR_6__15_), .CP(clk), .Q(y[15]) );
  DFQD1 yR_reg_7__14_ ( .D(yR_6__14_), .CP(clk), .Q(y[14]) );
  DFQD1 yR_reg_7__13_ ( .D(yR_6__13_), .CP(clk), .Q(y[13]) );
  DFQD1 yR_reg_7__12_ ( .D(yR_6__12_), .CP(clk), .Q(y[12]) );
  DFQD1 yR_reg_7__11_ ( .D(yR_6__11_), .CP(clk), .Q(y[11]) );
  DFQD1 yR_reg_7__10_ ( .D(yR_6__10_), .CP(clk), .Q(y[10]) );
  DFQD1 yR_reg_7__9_ ( .D(yR_6__9_), .CP(clk), .Q(y[9]) );
  DFQD1 yR_reg_7__8_ ( .D(yR_6__8_), .CP(clk), .Q(y[8]) );
  DFQD1 yR_reg_7__7_ ( .D(yR_6__7_), .CP(clk), .Q(y[7]) );
  DFQD1 yR_reg_7__6_ ( .D(yR_6__6_), .CP(clk), .Q(y[6]) );
  DFQD1 yR_reg_7__5_ ( .D(yR_6__5_), .CP(clk), .Q(y[5]) );
  DFQD1 yR_reg_7__4_ ( .D(yR_6__4_), .CP(clk), .Q(y[4]) );
  DFQD1 yR_reg_7__3_ ( .D(yR_6__3_), .CP(clk), .Q(y[3]) );
  DFQD1 yR_reg_7__2_ ( .D(yR_6__2_), .CP(clk), .Q(y[2]) );
  DFQD1 yR_reg_7__1_ ( .D(yR_6__1_), .CP(clk), .Q(y[1]) );
  DFQD1 yR_reg_7__0_ ( .D(yR_6__0_), .CP(clk), .Q(y[0]) );
  NR2D1 U143 ( .A1(n141), .A2(N48), .ZN(n153) );
  IND2D1 U144 ( .A1(N51), .B1(n137), .ZN(n134) );
  IND2D1 U145 ( .A1(N45), .B1(n155), .ZN(n145) );
  IND2D1 U146 ( .A1(N47), .B1(n143), .ZN(n141) );
  IND2D1 U147 ( .A1(N49), .B1(n153), .ZN(n139) );
  NR2D1 U148 ( .A1(n147), .A2(N44), .ZN(n155) );
  NR2D1 U149 ( .A1(n145), .A2(N46), .ZN(n143) );
  NR2D1 U150 ( .A1(n139), .A2(N50), .ZN(n137) );
  NR2D1 U151 ( .A1(N52), .A2(n134), .ZN(n133) );
  OR3D1 U152 ( .A1(N52), .A2(N53), .A3(n134), .Z(n49) );
  NR2D1 U153 ( .A1(n137), .A2(n183), .ZN(n136) );
  NR2D1 U154 ( .A1(n153), .A2(n183), .ZN(n152) );
  NR2D1 U155 ( .A1(n155), .A2(n183), .ZN(n154) );
  NR2D1 U156 ( .A1(n143), .A2(n183), .ZN(n142) );
  INVD1 U157 ( .I(n181), .ZN(n183) );
  ND2D1 U158 ( .A1(n181), .A2(n147), .ZN(n146) );
  ND2D1 U159 ( .A1(N57), .A2(n181), .ZN(n135) );
  ND2D1 U160 ( .A1(n181), .A2(n134), .ZN(n156) );
  ND2D1 U161 ( .A1(n181), .A2(n139), .ZN(n138) );
  ND2D1 U162 ( .A1(n181), .A2(n141), .ZN(n140) );
  ND2D1 U163 ( .A1(n181), .A2(n145), .ZN(n144) );
  ND2D1 U164 ( .A1(n181), .A2(n149), .ZN(n148) );
  ND2D1 U165 ( .A1(n181), .A2(n158), .ZN(n157) );
  ND2D1 U166 ( .A1(n181), .A2(n151), .ZN(n150) );
  ND2D1 U167 ( .A1(b[0]), .A2(b[7]), .ZN(n169) );
  ND2D1 U168 ( .A1(a[0]), .A2(a[7]), .ZN(n180) );
  ND2D1 U169 ( .A1(a[7]), .A2(n178), .ZN(n179) );
  ND2D1 U170 ( .A1(b[7]), .A2(n167), .ZN(n168) );
  ND2D1 U171 ( .A1(b[7]), .A2(n165), .ZN(n166) );
  ND2D1 U172 ( .A1(a[7]), .A2(n176), .ZN(n177) );
  NR2D1 U173 ( .A1(n163), .A2(n184), .ZN(n164) );
  NR2D1 U174 ( .A1(n174), .A2(n182), .ZN(n175) );
  ND2D1 U175 ( .A1(b[7]), .A2(n161), .ZN(n162) );
  ND2D1 U176 ( .A1(a[7]), .A2(n172), .ZN(n173) );
  INR3D0 U177 ( .A1(n170), .B1(a[6]), .B2(n182), .ZN(N21) );
  INR3D0 U178 ( .A1(n159), .B1(b[6]), .B2(n184), .ZN(N38) );
  XNR2D1 U179 ( .A1(n184), .A2(a[7]), .ZN(n181) );
  INVD1 U180 ( .I(b[7]), .ZN(n184) );
  INVD1 U181 ( .I(a[7]), .ZN(n182) );
  NR2D1 U182 ( .A1(n159), .A2(n184), .ZN(n160) );
  NR2D1 U183 ( .A1(n170), .A2(n182), .ZN(n171) );
  IND2D1 U184 ( .A1(b[4]), .B1(n163), .ZN(n161) );
  IND2D1 U185 ( .A1(a[4]), .B1(n174), .ZN(n172) );
  NR2D1 U186 ( .A1(n165), .A2(b[3]), .ZN(n163) );
  NR2D1 U187 ( .A1(n176), .A2(a[3]), .ZN(n174) );
  NR2D1 U188 ( .A1(n161), .A2(b[5]), .ZN(n159) );
  NR2D1 U189 ( .A1(n172), .A2(a[5]), .ZN(n170) );
endmodule

