// Example: testbench for traffic_lights

`timescale xms/xns //time unit and time precision 

module t_traffic_lights_framework;

//inputs
 reg t_clk;
 reg t_rst;

 
 //Outputs
 wire R_RNS, R_YNS, R_GNS, R_REW, R_YEW, R_GEW;


traffic_lights_block UT (); 

initial
 begin
  t_clk=1'b1;
  forever #delay t_clk=~t_clk; //clock period 
                            //set resolution to xms in modelsim
 end
 
initial
 begin
   t_rst=1'b1;
  #2 t_rst=1'b0;
  #60000 $stop; //stop simulation after 60002ms=60 seconds
 end


endmodule

