// Example: traffic_lights_top
module traffic_lights_block(clk , reset, RNS, YNS, GNS, REW, YEW, GEW);

input clk, reset;
output RNS, YNS, GNS, REW, YEW, GEW; //traffic lights

//instatiation traffic controller module
traffic_ct_block U1 (.clk(), .clr(), .lightRNS(), .lightYNS(), .lightGNS(), .lightREW(), .lightYEW(), .lightGEW());

endmodule
