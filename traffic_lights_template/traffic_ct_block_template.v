// Example: traffic lights controller
module traffic_ct_block (clk ,clr, lightRNS, lightYNS, lightGNS, lightREW, lightYEW, lightGEW);
input clk;
input clr;
output reg lightRNS, lightYNS, lightGNS, lightREW, lightYEW, lightGEW;
reg[x:0] state;
reg[x:0] count;

/* States: 
S0= Green_NS and Red_EW, S1=Yellow_NS and Red_EW, S2=Red_NS and Red_EW
S3= Red_NS and Green_EW, S4= Red_NS and Yellow_EW, S5= Red_NS and Red_EW */

parameter S0 = , S1 =, S2 = , // states 
S3 = , S4 = , S5 = ;



parameter SEC5 = , SEC1 = ; // delays 5sec and 1 sec

always @(posedge clk or posedge clr)
  begin
   if (clr == 1)
     begin
        state <= S0;
        count<=0;
     end   
   else 
      begin
       state<= next_state;
       count<= next_count;
     end
  end
  
  
// next state and next count logic            
always@(state, count)
  begin
     case(state)
          S0: 
          S1: 
          S2: 
          S3: 
          S4: 
          S5: 
         default: next_state = S0;
      endcase
     end



  
//output logic
always @(*)
   begin
    lightGNS=; lightYNS=; lightRNS=; lightGEW=; lightYEW=; lightREW=; //Initialization
      case(state)
          S0: 
          S1: 
          S2: 
          S3: 
          S4: 
          S5: 
          default: 
     endcase
   end

endmodule

